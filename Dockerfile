FROM friendica/server:2020.03-dev
ADD cron.sh /cron.sh
CMD ["/bin/bash", "-c", "set -e && /cron.sh"]
